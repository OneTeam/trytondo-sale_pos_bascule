# This file is part of sale_pos module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields



class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    @fields.depends('product', 'unit', 'sale', '_parent_sale.sale_device',
                    '_parent_sale.party', '_parent_sale.invoice_party',
                    methods=['compute_taxes', 'compute_unit_price',
                             'on_change_with_amount'])

    def on_change_product(self):
        if not self.product:
            self.quantity = None
            self.unit = None
            return

        pool = Pool()

        bascule = self.sale.sale_device.bascule.read_measure()
        if bascule:
            if self.product.template.sale_uom.category.name == 'Peso' and \
               bascule > 0:
                unit = bascule/1000
                rate = self.product.template.sale_uom.rate
                self.quantity = unit * rate
                    
        party = None
        if self.sale:
            party = self.sale.invoice_party or self.sale.party

        # Set taxes before unit_price to have taxes in context of sale price
        self.taxes = self.compute_taxes(party)

        category = self.product.sale_uom.category
        if not self.unit or self.unit.category != category:
            self.unit = self.product.sale_uom
            self.unit_digits = self.product.sale_uom.digits

        self.unit_price = self.compute_unit_price()

        self.type = 'line'
        self.amount = self.on_change_with_amount()

