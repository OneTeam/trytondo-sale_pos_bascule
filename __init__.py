# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import bascule
from . import device
from . import sale

__all__ = ['register']

def register():
    Pool.register(
        bascule.Bascule,
        device.SaleDevice,
        sale.SaleLine,
        module='sale_pos_bascule', type_='model')
