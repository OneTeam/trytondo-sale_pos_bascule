import socket
import serial
import os
import time
import warnings


TCP_HOST = ''
TCP_PORT = 5000

TTY_PORT = '/dev/ttyUSB0'
TTY_BAUD = 9600

class Error_Conn(RuntimeError):
    pass

def value_conn(conn):
    conn.read(6)
    b_value = conn.readline() or b'0.0'
    if b_value not in [b'0.0']:
        try:
            value = b_value.decode().split()
        except Exception as e:
            warnings.warn(e.message)
            raise Error_Conn(e.message)
                                    
        if len(value) == 2 and value[0] in ['+','ST,GS,+','US,GS,+']:
            if value[1].index("lb"):
                print("[+] Reading from bascule: {0}".format(b_value))
                return value[1].replace("lb", "", 1)
    print("[+] Reading from bascule: {0}".format(b_value)) 
    


def get_bascule_value():
    if os.path.exists(TTY_PORT):
        '''Read from serial'''
        with serial.Serial(TTY_PORT, TTY_BAUD, parity=serial.PARITY_NONE,\
                           stopbits=serial.STOPBITS_ONE,\
                           timeout=0) as conn:
            try:
                return value_conn(conn)
            except Error_Conn:
                pass

    else:
        print("[-] Not TTY Port")
        print("[-] Not bascule measure")
        return 0


def server(host=TCP_HOST, port=TCP_PORT):
    '''Runs a listener tcp socker server'''
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((host, port))
        sock.listen(10)
        print("[+] Listening on {0}:{1}".format(host, port))

        while True:
            conn, addr = sock.accept()
            print("[+] Connecting by {0}:{1}".format(addr[0], addr[1]))

            while True:
                request = conn.recv(4096)

                if not request:
                    print("[-] Not Received")
                    break

                print("[+] Received", repr(request.decode('utf-8')))

                response = get_bascule_value()
                for i in range(0,4):
                    if response in [b'0.0', '0', None]:
                        time.sleep(0.7)
                        response = get_bascule_value()
                    else:
                        break
                print(response)
                
                if response == None:
                    response = '0'
                    
                conn.sendall(response.encode())
                print("[+] Sending to {0}:{1}".format(addr[0], addr[1]))
            conn.close()


if __name__ == "__main__":
    server()
