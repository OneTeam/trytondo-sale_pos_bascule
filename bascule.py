# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import socket
from trytond.model import ModelSQL, ModelView, fields, DeactivableMixin
from trytond.pyson import Eval
from trytond.config import parse_uri, get_hostname, get_port

__all__ = ['Bascule']


class Bascule(DeactivableMixin, ModelSQL, ModelView):
    '''Sale Bascule'''
    __name__ = 'sale.bascule'

    name = fields.Char('Name', required=True, states={
            'readonly': ~Eval('active')},
        depends=['active'])
    uri = fields.Char('Uri', required=True, states={
            'readonly': ~Eval('active')
        },
        depends=['active'],
        help='Determines the connection info (i.e. tcp://ip:port)')

    def read_measure(self):
        uri = parse_uri(self.uri)

        host = get_hostname(uri.netloc)
        port = get_port(uri.netloc)
       
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.settimeout(10)
            data = 0
            try:
                s.connect((host, port))
                s.send(b'GET')
                data = s.recv(1024).decode()
                s.close()
            except:
                s.close()
                return float(0)
        
        try:
            return float(data)
        except:
            return float(0)
