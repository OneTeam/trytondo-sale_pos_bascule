# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class SaleDevice(metaclass=PoolMeta):
    'Sale Device Configuration'
    __name__ = 'sale.device'
    bascule = fields.Many2One('sale.bascule',
                              'Bascule',
                              required=True,
                              select=True)
